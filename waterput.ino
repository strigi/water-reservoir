#include <NewPing.h>
#include <LiquidCrystal.h>

const float TANK_HEIGHT = 1.52;
const float TANK_RADIUS = 0.78;
const float SENSOR_MOUNT_ABOVE_MAXIMUM_SURFACE_LEVEL = 0.18;

const int SONAR_SENSOR_TRIGGER_PIN = 12;
const int SONAR_SENSOR_ECHO_PIN = 11;
const int LCD_RS_PIN = 7;
const int LCD_ENABLE_PIN = 6;
const int LCD_D4_PIN = 5;
const int LCD_D5_PIN = 4;
const int LCD_D6_PIN = 3;
const int LCD_D7_PIN = 2;
const int LCD_DISPLAY_COLUMNS = 16;
const int LCD_DISPLAY_ROWS = 2;
const int REFRESH_DELAY = 5000;

const uint8_t CUSTOM_CHARACTER_HEART = 0;

const int CM = 100; // Centimeters

NewPing sonar(SONAR_SENSOR_TRIGGER_PIN, SONAR_SENSOR_ECHO_PIN, (TANK_HEIGHT + SENSOR_MOUNT_ABOVE_MAXIMUM_SURFACE_LEVEL) * CM);
LiquidCrystal lcd(LCD_RS_PIN, LCD_ENABLE_PIN, LCD_D4_PIN, LCD_D5_PIN, LCD_D6_PIN, LCD_D7_PIN);

float maximumVolume;

void setup() {
  byte heartCharacterPixelMap[8] = {
    0b00000,
    0b01010,
    0b11111,
    0b11111,
    0b11111,
    0b01110,
    0b00100,
    0b00000
  };
  lcd.createChar(CUSTOM_CHARACTER_HEART, heartCharacterPixelMap);
  lcd.begin(LCD_DISPLAY_COLUMNS, LCD_DISPLAY_ROWS);
  Serial.begin(9600);

  maximumVolume = calculateTankVolume(TANK_HEIGHT);
  
  Serial.println("Program parameters:");  
  serialPrintNumberFloat("\t- Tank radius: ", TANK_RADIUS, + "m\n");
  serialPrintNumberFloat("\t- Tank height: ", TANK_HEIGHT, + "m\n");
  serialPrintNumberFloat("\t- Sensor mount above maximum surface level: ", SENSOR_MOUNT_ABOVE_MAXIMUM_SURFACE_LEVEL, + "m\n");
  serialPrintNumberFloat("\t- Update values every: ", REFRESH_DELAY, + "ms\n");
  Serial.println();
  serialPrintNumberFloat("\t- Maximum volume: ", maximumVolume, "L\n");
  Serial.println("---");

  displayWelcomeMessage();
  delay(2000);
}

void loop() {  
  const float currentDepth = measureWaterDepthMeters();
  if(currentDepth < 0) {
    displayError("Buiten bereik");
  } else {    
    const float currentVolume = calculateTankVolume(currentDepth);
    const float currentFillPercentage = (currentVolume / maximumVolume) * 100;
    displayValues(currentVolume, currentFillPercentage);
  }
  Serial.println("---");
  delay(REFRESH_DELAY);
}

/**
 * Calculates the depth of the water (from the bottom to the surface) in meters.
 * This is done by measuring the distance to the surface, and than subtracting that from the known distance of the sensor to the bottom of the well.
 */
float measureWaterDepthMeters() {
  float distanceToBottom = TANK_HEIGHT + SENSOR_MOUNT_ABOVE_MAXIMUM_SURFACE_LEVEL;
  float distanceToSurface = sonar.convert_cm(sonar.ping_median()) / 100.0;
  float depth = distanceToBottom - distanceToSurface;
  serialPrintNumberFloat("Distance to surface: ", distanceToSurface, "m (varies)\n");
  serialPrintNumberFloat("Distance to bottom: ", distanceToBottom, "m (fixed)\n");    
  serialPrintNumberFloat("Current depth: ", depth, "m\n");
  if(sonar.check_timer()) {
    return depth;
  } else {
    Serial.println("Measured value out of range. Reporting as error.");
    return -1; // Out of range error
  }
}

/**
 * Calculates the volume in liters (cm³) of the tank with predefined radius, with the given depth in meters
 * @param depth the depth of the tank in meters.
 * @returns The volume of the tank cylinder in liters or cm³, as a decimal.
 * @see TANK_RADIUS
 */
float calculateTankVolume(float depth) {
  return TANK_RADIUS * TANK_RADIUS * PI * depth * 1000;
}

void displayError(const char* errorMessage) {
  lcd.clear();
  lcd.setCursor(0, 0);  
  const char* errorTag = "FOUT: ";
  lcd.print(errorTag);
  lcd.setCursor(0, 1);
  lcd.print(errorMessage);
  Serial.print(errorTag);
  Serial.println(errorMessage);
}

void displayValues(float volumeLiters, float volumePercentage) {
  serialPrintNumberFloat("Current volume in liters: ", volumeLiters, "L ");
  serialPrintNumberFloat("(", volumePercentage, "%)\n");
  
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Niveau waterput:");  
  lcd.setCursor(0, 1);  
  lcd.print(round(volumeLiters));
  lcd.print("L");  
  int space = 3;
  if(volumePercentage >= 100) {
    space++;
  } else if(volumePercentage < 10) {
    space--;
  }
  lcd.setCursor(LCD_DISPLAY_COLUMNS - space, 1);
  lcd.print(round(volumePercentage));
  lcd.print("%");
}

void displayWelcomeMessage() {
  lcd.clear();
  lcd.print("Voor Mama & Papa");
  lcd.setCursor(0, 1);
  lcd.print("met ");
  lcd.write(CUSTOM_CHARACTER_HEART);
  lcd.print(" van Kevin");  
}

void serialPrintNumberFloat(const char* prefix, float number, const char* suffix) {
  Serial.print(prefix);
  Serial.print(number);
  Serial.print(suffix);
}
